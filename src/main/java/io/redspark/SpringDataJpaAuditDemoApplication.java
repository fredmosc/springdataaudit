package io.redspark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import io.redspark.audit.SpringSecurityAuditorAware;
import io.redspark.domain.User;

@SpringBootApplication
@EnableJpaAuditing
public class SpringDataJpaAuditDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaAuditDemoApplication.class, args);
	}
	
	@Bean
	public AuditorAware<User> auditorProvider(){
		return new SpringSecurityAuditorAware();
	}
	
}
